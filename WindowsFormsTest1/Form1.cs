﻿
using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Excel = Microsoft.Office.Interop.Excel;
using ExcelAutoFormat = Microsoft.Office.Interop.Excel.XlRangeAutoFormat;

namespace WindowsFormsTest1
{
    public partial class Form1 : Form
    {

        private static Logger logger = LogManager.GetLogger("logger");

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'tESTDBDataSet.PERSON' table. You can move, or remove it, as needed.
            //this.pERSONTableAdapter.Fill(this.tESTDBDataSet.PERSON);
            this.exportExcelFile2();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.exportexcelfile();
        }

        //private void fillByToolStripButton_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        this.pERSONTableAdapter.FillBy(this.tESTDBDataSet.PERSON);
        //    }
        //    catch (System.Exception ex)
        //    {
        //        System.Windows.Forms.MessageBox.Show(ex.Message);
        //    }

        //}

        private void exportexcelfile()
        {
            //export employee details to excel.

           logger.Info("test start log");

            Cursor.Current = Cursors.WaitCursor;

            string sEmpList = "";
            int iRowCnt = 0;

            string sCon = ConfigurationManager.ConnectionStrings["Syndome_PayrollConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(sCon))
            {
                // SQL QUERY TO FETCH RECORDS FROM THE DATABASE.
                String sSql = "SELECT * FROM dbo.VW_HR_EMPINFO ";

                SqlDataAdapter sda = new SqlDataAdapter(sSql, con);
                try
                {
                    DataTable dt = new DataTable();
                    sda.Fill(dt);

                    /********************************start ***************************/

                    logger.Info("test111");
                    /*****************************************************************/

                    string path = ConfigurationManager.AppSettings["SaveToFolder"].ToString();// "D:\\temp\\";

                    if (!Directory.Exists(path))   // CHECK IF THE FOLDER EXISTS. IF NOT, CREATE A NEW FOLDER.
                    {
                        Directory.CreateDirectory(path);
                    }

                    File.Delete(path + "test5.xlsx"); // DELETE THE FILE BEFORE CREATING A NEW ONE.

                    // ADD A WORKBOOK USING THE EXCEL APPLICATION.
                    Excel.Application xlAppToUpload = new Excel.Application();
                    xlAppToUpload.Workbooks.Add("");


                    logger.Info("test2222");
                    //Excel.Worksheet xlWorkSheetToUpload = default(Excel.Worksheet);
                    //xlWorkSheetToUpload = (Excel.Worksheet)xlAppToUpload.Sheets["Sheet1"];


                    /*******************template ****************/
                    string strTemplate = path + "Template_TEST.xlsx";


                    Excel.Workbook xlWorkBook = xlAppToUpload.Workbooks.Open(strTemplate, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                    Excel.Worksheet xlWorkSheetToUpload = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                    //xlWorkSheetToUpload.Name = "My Sheet";


                    //var worksheet = xlAppToUpload.Worksheets.Add("AddGroup");
                    //Excel.Range testRange = xlWorkSheetToUpload.get_Range("A12", "A14");

                    logger.Info("test333");

                    //testRange.Group(Type.Missing, Type.Missing, Type.Missing, Type.Missing);


                    xlWorkSheetToUpload.Cells[12, 1] = "DataCol 1";
                    xlWorkSheetToUpload.Cells[12, 2] = "DataCol 2";
                    xlWorkSheetToUpload.Cells[12, 3] = "DataCol 3";
                    xlWorkSheetToUpload.Cells[12, 4] = "DataCol 4";
                    xlWorkSheetToUpload.Cells[12, 5] = "DataCol 5";

                    xlWorkSheetToUpload.Cells[12, 6] = "DataCol 6";

                    xlWorkSheetToUpload.Cells[12, 7] = "DataCol 7";

                    xlWorkSheetToUpload.Cells[12, 8] = "DataCol 8";

                    xlWorkSheetToUpload.Cells[12, 9] = "DataCol 9";
                    /*******************template ****************/

                    //xlWorkSheetToUpload.get_Range("A12", "B12").BorderAround2(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);


                    ((Excel.Range)xlWorkSheetToUpload.get_Range("A12", "I14")).Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                    ((Excel.Range)xlWorkSheetToUpload.get_Range("A12", "I14")).Cells.Value = "-";

                    xlWorkSheetToUpload.get_Range("A12", "I14").Font.Name = "Tahoma";
                    xlWorkSheetToUpload.get_Range("A12", "I14").Font.Size = "15";
                    xlWorkSheetToUpload.get_Range("A12", "I14").VerticalAlignment = -4108;
                    xlWorkSheetToUpload.get_Range("A12", "I14").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheetToUpload.get_Range("A12", "I14").Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Aqua);
                    xlWorkSheetToUpload.get_Range("A12", "I14").Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    xlWorkSheetToUpload.get_Range("A12", "I14").Font.Bold = true;


                    logger.Info("test444");

                    if (dt.Rows.Count > 0)
                    {
                        iRowCnt = 8;                      // ROW AT WHICH PRINT WILL START.

                        // SHOW THE HEADER BOLD AND SET FONT AND SIZE.
                        //xlWorkSheetToUpload.Cells[1, 1].value = "Employee Details";
                        //xlWorkSheetToUpload.Cells[1, 1].FONT.NAME = "Calibri";
                        //xlWorkSheetToUpload.Cells[1, 1].Font.Bold = true;
                        //xlWorkSheetToUpload.Cells[1, 1].Font.Size = 20;

                        // MERGE CELLS OF THE HEADER.
                        xlWorkSheetToUpload.Range["A1:E1"].MergeCells = true;

                        // SHOW COLUMNS ON THE TOP.
                        xlWorkSheetToUpload.Cells[iRowCnt - 1, 1].value = "NO";
                        xlWorkSheetToUpload.Cells[iRowCnt - 1, 2].value = "FIRSTNAME";
                        xlWorkSheetToUpload.Cells[iRowCnt - 1, 3].value = "LASTNAME";
                        xlWorkSheetToUpload.Cells[iRowCnt - 1, 4].value = "IDCARD";
                        //xlWorkSheetToUpload.Cells[iRowCnt - 1, 5].value = "AGE";


                        //DataRow[] rows222 = dt.Select("[AGE] = 12");
                        //for (int j = 0; j < rows222.Length; j++)
                        //{
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 1].value = j;
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 2].value = rows222[j]["FIRSTNAME"];
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 3].value = rows222[j]["LASTNAME"];
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 4].value = rows222[j]["IDCARD"];
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 5].value = rows222[j]["AGE"];

                        //    iRowCnt = iRowCnt + 1;
                        //}


                        //logger.Error("testest error");
                        //// NOW WRITE DATA TO EACH CELL.
                        //for (var i = 0; i <= dt.Rows.Count - 1; i++)
                        //{
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 1].value = i;
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 2].value = dt.Rows[i]["FIRSTNAME"];
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 3].value = dt.Rows[i]["LASTNAME"];
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 4].value = dt.Rows[i]["IDCARD"];
                        //    xlWorkSheetToUpload.Cells[iRowCnt, 5].value = dt.Rows[i]["AGE"];

                        //    iRowCnt = iRowCnt + 1;
                        //}


                        xlWorkSheetToUpload.Rows["12:20"].Group();


                        logger.Info("save in success");
                        // FINALLY, FORMAT THE EXCEL SHEET USING EXCEL'S AUTOFORMAT FUNCTION.
                        //xlAppToUpload.ActiveCell.Worksheet.Cells[4, 1].AutoFormat(ExcelAutoFormat.xlRangeAutoFormatList3);




                        // SAVE THE FILE IN A FOLDER.
                        xlWorkSheetToUpload.SaveAs(path + "test5.xlsx");

                        // CLEAR.
                        xlAppToUpload.Workbooks.Close();
                        xlAppToUpload.Quit();
                        xlAppToUpload = null;
                        xlWorkSheetToUpload = null;


                        int[] deptcode = { 1200, 1300, 1400, 1500 };

                        string test = "";
                        for (int i = 0; i < deptcode.Length; i++)
                        {
                            test += " , " + deptcode[i];
                        }

                        MessageBox.Show("OK " + test, "Success",
                      MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "You got an Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sda.Dispose();
                    sda = null;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.exportExcelFile2();
        }

        private void exportExcelFile2()
        {
            // EXPORT EMPLOYEE DETAILS TO EXCEL.

            Cursor.Current = Cursors.WaitCursor;

            string sEmpList = "";
            int iRowCnt = 0;

            string sCon = ConfigurationManager.ConnectionStrings["Syndome_PayrollConnectionString"].ConnectionString;

            using (SqlConnection con = new SqlConnection(sCon))
            {





                // SQL QUERY TO FETCH RECORDS FROM THE DATABASE.
                //String sSql = "SELECT TOP 10 * FROM dbo.VW_HR_EMPINFO ";
                SqlDataAdapter sda = new SqlDataAdapter();
                try
                {


                    /********************************start ***************************/


                    /*****************************************************************/

                    string path = ConfigurationManager.AppSettings["SaveToFolder"].ToString();// "D:\\temp\\";

                    if (!Directory.Exists(path))   // CHECK IF THE FOLDER EXISTS. IF NOT, CREATE A NEW FOLDER.
                    {
                        Directory.CreateDirectory(path);
                    }

                    File.Delete(path + "Monly_Test.xlsx"); // DELETE THE FILE BEFORE CREATING A NEW ONE.

                    // ADD A WORKBOOK USING THE EXCEL APPLICATION.
                    Excel.Application xlAppToUpload = new Excel.Application();
                    xlAppToUpload.Workbooks.Add("");


                    //Excel.Worksheet xlWorkSheetToUpload = default(Excel.Worksheet);
                    //xlWorkSheetToUpload = (Excel.Worksheet)xlAppToUpload.Sheets["Sheet1"];


                    /*******************template ****************/
                    string strTemplate = path + "Template_TEST.xlsx";


                    Excel.Workbook xlWorkBook = xlAppToUpload.Workbooks.Open(strTemplate, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);

                    Excel.Worksheet xlWorkSheetToUpload = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);



                    String master_deptsql = "SELECT * FROM dbo.MASTER_DEPT ORDER BY SEQUENCE";
                    sda = new SqlDataAdapter(master_deptsql, con);
                    DataTable master_deptdt = new DataTable();
                    sda.Fill(master_deptdt);
                    DataRow[] master_row = master_deptdt.Select("MASTER_CODE = '0000' ");
                    iRowCnt = 4;
                    int num_row1 = 0;
                    int num_row2 = 0;
                    int rowHeadGroup = 0;

                    ArrayList list = new ArrayList();
                    ArrayList listHead = new ArrayList();


                    //xlWorkSheetToUpload.get_Range("A12", "B12").BorderAround2(Excel.XlLineStyle.xlContinuous, Excel.XlBorderWeight.xlThin, Excel.XlColorIndex.xlColorIndexAutomatic, Excel.XlColorIndex.xlColorIndexAutomatic);


                    //((Excel.Range)xlWorkSheetToUpload.get_Range("CK6", "CQ13")).Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                    xlWorkSheetToUpload.get_Range("A" + 3, "CQ" + 3).Cells.Value = "0";

                    //xlWorkSheetToUpload.get_Range("A" + 3, "CQ" + 3).Font.Name = "A";
                    //xlWorkSheetToUpload.get_Range("CK6", "CQ13").Font.Size = "15";
                    //xlWorkSheetToUpload.get_Range("A12", "I14").VerticalAlignment = -4108;
                    xlWorkSheetToUpload.get_Range("A" + 3, "CQ" + 3).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    xlWorkSheetToUpload.get_Range("A" + 3, "CQ" + 3).Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                    xlWorkSheetToUpload.get_Range("A" + 3, "CQ" + 3).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                    //xlWorkSheetToUpload.get_Range("CK6", "CQ13").Font.Bold = true;

                    //xlWorkSheetToUpload.Cells["B", 3].value = "รวม";

                    xlWorkSheetToUpload.Cells[3, 5].Formula = "=SUM(L" + 3 + ",S" + 3 + ",Z" + 3 + ",AG" + 3 + ",AN" + 3 + ",AU" + 3 + ",BB" + 3 + ",BI" + 3 + ",BP" + 3 + ",BW" + 3 + ",CD" + 3 + ",CK" + 3 + ")";
                    xlWorkSheetToUpload.Cells[3, 6].Formula = "=SUM(M" + 3 + ",T" + 3 + ",AA" + 3 + ",AH" + 3 + ",AO" + 3 + ",AV" + 3 + ",BC" + 3 + ",BJ" + 3 + ",BQ" + 3 + ",BX" + 3 + ",CE" + 3 + ",CL" + 3 + ")";
                    xlWorkSheetToUpload.Cells[3, 7].Formula = "=SUM(N" + 3 + ",U" + 3 + ",AB" + 3 + ",AI" + 3 + ",AP" + 3 + ",AW" + 3 + ",BD" + 3 + ",BK" + 3 + ",BR" + 3 + ",BY" + 3 + ",CF" + 3 + ",CM" + 3 + ")";
                    xlWorkSheetToUpload.Cells[3, 8].Formula = "=SUM(O" + 3 + ",V" + 3 + ",AC" + 3 + ",AJ" + 3 + ",AQ" + 3 + ",AX" + 3 + ",BE" + 3 + ",BL" + 3 + ",BS" + 3 + ",BZ" + 3 + ",CG" + 3 + ",CN" + 3 + ")";
                    xlWorkSheetToUpload.Cells[3, 9].Formula = "=SUM(P" + 3 + ",W" + 3 + ",AD" + 3 + ",AK" + 3 + ",AR" + 3 + ",AY" + 3 + ",BF" + 3 + ",BM" + 3 + ",BT" + 3 + ",CA" + 3 + ",CH" + 3 + ",CO" + 3 + ")";
                    xlWorkSheetToUpload.Cells[3, 10].Formula = "=SUM(Q" + 3 + ",X" + 3 + ",AE" + 3 + ",AL" + 3 + ",AS" + 3 + ",AZ" + 3 + ",BG" + 3 + ",BN" + 3 + ",BU" + 3 + ",CB" + 3 + ",CI" + 3 + ",CP" + 3 + ")";
                    xlWorkSheetToUpload.Cells[3, 11].Formula = "=SUM(R" + 3 + ",Z" + 3 + ",AF" + 3 + ",AM" + 3 + ",AT" + 3 + ",BA" + 3 + ",BH" + 3 + ",BO" + 3 + ",BV" + 3 + ",CC" + 3 + ",CJ" + 3 + ",CQ" + 3 + ")";

                    for (int i = 0; i < master_row.Length; i++)
                    {
                        xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
                        xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Gray);
                        xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                        xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Value = "0";


                        xlWorkSheetToUpload.Cells[iRowCnt, 1].value = master_row[i]["DEPT_CODE"];
                        xlWorkSheetToUpload.Cells[iRowCnt, 2].value = master_row[i]["DEPT_NAME"]; //บริหาร

                        xlWorkSheetToUpload.Cells[iRowCnt, 5].Formula = "=SUM(L" + iRowCnt + ",S" + iRowCnt + ",Z" + iRowCnt + ",AG" + iRowCnt + ",AN" + iRowCnt + ",AU" + iRowCnt + ",BB" + iRowCnt + ",BI" + iRowCnt + ",BP" + iRowCnt + ",BW" + iRowCnt + ",CD" + iRowCnt + ",CK" + iRowCnt + ")";
                        xlWorkSheetToUpload.Cells[iRowCnt, 6].Formula = "=SUM(M" + iRowCnt + ",T" + iRowCnt + ",AA" + iRowCnt + ",AH" + iRowCnt + ",AO" + iRowCnt + ",AV" + iRowCnt + ",BC" + iRowCnt + ",BJ" + iRowCnt + ",BQ" + iRowCnt + ",BX" + iRowCnt + ",CE" + iRowCnt + ",CL" + iRowCnt + ")";
                        xlWorkSheetToUpload.Cells[iRowCnt, 7].Formula = "=SUM(N" + iRowCnt + ",U" + iRowCnt + ",AB" + iRowCnt + ",AI" + iRowCnt + ",AP" + iRowCnt + ",AW" + iRowCnt + ",BD" + iRowCnt + ",BK" + iRowCnt + ",BR" + iRowCnt + ",BY" + iRowCnt + ",CF" + iRowCnt + ",CM" + iRowCnt + ")";
                        xlWorkSheetToUpload.Cells[iRowCnt, 8].Formula = "=SUM(O" + iRowCnt + ",V" + iRowCnt + ",AC" + iRowCnt + ",AJ" + iRowCnt + ",AQ" + iRowCnt + ",AX" + iRowCnt + ",BE" + iRowCnt + ",BL" + iRowCnt + ",BS" + iRowCnt + ",BZ" + iRowCnt + ",CG" + iRowCnt + ",CN" + iRowCnt + ")";
                        xlWorkSheetToUpload.Cells[iRowCnt, 9].Formula = "=SUM(P" + iRowCnt + ",W" + iRowCnt + ",AD" + iRowCnt + ",AK" + iRowCnt + ",AR" + iRowCnt + ",AY" + iRowCnt + ",BF" + iRowCnt + ",BM" + iRowCnt + ",BT" + iRowCnt + ",CA" + iRowCnt + ",CH" + iRowCnt + ",CO" + iRowCnt + ")";
                        xlWorkSheetToUpload.Cells[iRowCnt, 10].Formula = "=SUM(Q" + iRowCnt + ",X" + iRowCnt + ",AE" + iRowCnt + ",AL" + iRowCnt + ",AS" + iRowCnt + ",AZ" + iRowCnt + ",BG" + iRowCnt + ",BN" + iRowCnt + ",BU" + iRowCnt + ",CB" + iRowCnt + ",CI" + iRowCnt + ",CP" + iRowCnt + ")";
                        xlWorkSheetToUpload.Cells[iRowCnt, 11].Formula = "=SUM(R" + iRowCnt + ",Z" + iRowCnt + ",AF" + iRowCnt + ",AM" + iRowCnt + ",AT" + iRowCnt + ",BA" + iRowCnt + ",BH" + iRowCnt + ",BO" + iRowCnt + ",BV" + iRowCnt + ",CC" + iRowCnt + ",CJ" + iRowCnt + ",CQ" + iRowCnt + ")";

                        listHead.Add(iRowCnt);
                        num_row1 = iRowCnt;
                        iRowCnt = iRowCnt + 1;
                        list.Clear();
                        rowHeadGroup = 0;

                        DataRow[] dept_row = master_deptdt.Select("MASTER_CODE = '" + master_row[i]["DEPT_CODE"] + "' ");

                        for (int j = 0; j < dept_row.Length; j++) 
                        {
                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.DarkGray);
                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Value = "0";

                            xlWorkSheetToUpload.Cells[iRowCnt, 1].value = dept_row[j]["DEPT_CODE"];
                            xlWorkSheetToUpload.Cells[iRowCnt, 2].value = dept_row[j]["DEPT_NAME"]; //แผนก

                            xlWorkSheetToUpload.Cells[iRowCnt, 5].Formula = "=SUM(L" + iRowCnt + ",S" + iRowCnt + ",Z" + iRowCnt + ",AG" + iRowCnt + ",AN" + iRowCnt + ",AU" + iRowCnt + ",BB" + iRowCnt + ",BI" + iRowCnt + ",BP" + iRowCnt + ",BW" + iRowCnt + ",CD" + iRowCnt + ",CK" + iRowCnt + ")";
                            xlWorkSheetToUpload.Cells[iRowCnt, 6].Formula = "=SUM(M" + iRowCnt + ",T" + iRowCnt + ",AA" + iRowCnt + ",AH" + iRowCnt + ",AO" + iRowCnt + ",AV" + iRowCnt + ",BC" + iRowCnt + ",BJ" + iRowCnt + ",BQ" + iRowCnt + ",BX" + iRowCnt + ",CE" + iRowCnt + ",CL" + iRowCnt + ")";
                            xlWorkSheetToUpload.Cells[iRowCnt, 7].Formula = "=SUM(N" + iRowCnt + ",U" + iRowCnt + ",AB" + iRowCnt + ",AI" + iRowCnt + ",AP" + iRowCnt + ",AW" + iRowCnt + ",BD" + iRowCnt + ",BK" + iRowCnt + ",BR" + iRowCnt + ",BY" + iRowCnt + ",CF" + iRowCnt + ",CM" + iRowCnt + ")";
                            xlWorkSheetToUpload.Cells[iRowCnt, 8].Formula = "=SUM(O" + iRowCnt + ",V" + iRowCnt + ",AC" + iRowCnt + ",AJ" + iRowCnt + ",AQ" + iRowCnt + ",AX" + iRowCnt + ",BE" + iRowCnt + ",BL" + iRowCnt + ",BS" + iRowCnt + ",BZ" + iRowCnt + ",CG" + iRowCnt + ",CN" + iRowCnt + ")";
                            xlWorkSheetToUpload.Cells[iRowCnt, 9].Formula = "=SUM(P" + iRowCnt + ",W" + iRowCnt + ",AD" + iRowCnt + ",AK" + iRowCnt + ",AR" + iRowCnt + ",AY" + iRowCnt + ",BF" + iRowCnt + ",BM" + iRowCnt + ",BT" + iRowCnt + ",CA" + iRowCnt + ",CH" + iRowCnt + ",CO" + iRowCnt + ")";
                            xlWorkSheetToUpload.Cells[iRowCnt, 10].Formula = "=SUM(Q" + iRowCnt + ",X" + iRowCnt + ",AE" + iRowCnt + ",AL" + iRowCnt + ",AS" + iRowCnt + ",AZ" + iRowCnt + ",BG" + iRowCnt + ",BN" + iRowCnt + ",BU" + iRowCnt + ",CB" + iRowCnt + ",CI" + iRowCnt + ",CP" + iRowCnt + ")";
                            xlWorkSheetToUpload.Cells[iRowCnt, 11].Formula = "=SUM(R" + iRowCnt + ",Z" + iRowCnt + ",AF" + iRowCnt + ",AM" + iRowCnt + ",AT" + iRowCnt + ",BA" + iRowCnt + ",BH" + iRowCnt + ",BO" + iRowCnt + ",BV" + iRowCnt + ",CC" + iRowCnt + ",CJ" + iRowCnt + ",CQ" + iRowCnt + ")";

                            list.Add(iRowCnt);

                            int row_month = 0;
                            num_row2 = iRowCnt;
                            row_month = iRowCnt;

                            rowHeadGroup += 1;
                            int row_group = 0;
                            String sql2 = "SELECT a.PRS_NO," +
                                " b.EMP_INTL + ' ' + b.EMP_NAME + ' ' + b.EMP_SURNME AS NAME," +
                                " DEPT_CODE" +
                                " FROM" +
                                " dbo.PERSONALINFO AS a" +
                                " LEFT OUTER JOIN dbo.EMPFILE AS b ON a.PRS_EMP = b.EMP_KEY" +
                                " LEFT OUTER JOIN dbo.DEPTTAB AS DEPT ON a.PRS_DEPT = DEPT.DEPT_KEY" +
                                " WHERE DEPT_CODE<> 3000 AND(DEPT_CODE <> '0012' OR PRS_NO = 'SV0004')" +
                                " AND(DEPT_CODE <> '1800' OR PRS_NO <> 'PAT20')" +
                                " AND(DEPT_CODE NOT IN (3001,1100,2505,2503,2800,2504,7000,9000,9001 ))" +
                                " AND(PRS_NO NOT IN('RD0005', 'IT0007', 'IT0004', 'IT0005'))" +
                                " AND(PRS_NO NOT IN('DR0005', 'DR0007', 'DR0009', 'DR0008', 'DR0018','RD0004','SV0001'))" +
                                " AND(DEPT_CODE <> '0015' OR PRS_NO = 'BD1401')" +
                                " AND(DEPT_CODE = '" + dept_row[j]["DEPT_CODE"].ToString() + "')";

                            sda = new SqlDataAdapter(sql2, con);
                            DataTable dt2 = new DataTable();
                            sda.Fill(dt2);
                            if (dt2.Rows.Count > 0)
                            {

                                for (int kt = 0; kt < dt2.Rows.Count; kt++)
                                {
                                   
                                    iRowCnt = iRowCnt + 1;
                                    //row_month = iRowCnt;
                                    row_group++;
                                    rowHeadGroup++;

                                    //Default 0
                                    xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Value = "0";

                                    for (int month = 1; month <= 12; month++)
                                    {
                                       
                                        String sSql = this.sqlquery(dept_row[j]["DEPT_CODE"].ToString(), month, 2023, dt2.Rows[kt]["PRS_NO"].ToString());

                                        sda = new SqlDataAdapter(sSql, con);
                                        DataTable dt = new DataTable();
                                        sda.Fill(dt);
                                        if (dt.Rows.Count > 0)
                                        {
                                           // iRowCnt = row_month;
                                            //row_group = dt.Rows.Count;
                                            //rowHeadGroup += dt.Rows.Count;

                                            xlWorkSheetToUpload.Cells[num_row2, 96 - (7 * month)].Formula = this.rendervaluefrommonth((num_row2 + 1), (dt2.Rows.Count - 1), month, 96 - (7 * month));
                                            xlWorkSheetToUpload.Cells[num_row2, 97 - (7 * month)].Formula = this.rendervaluefrommonth((num_row2 + 1), (dt2.Rows.Count - 1), month, 97 - (7 * month));
                                            xlWorkSheetToUpload.Cells[num_row2, 98 - (7 * month)].Formula = this.rendervaluefrommonth((num_row2 + 1), (dt2.Rows.Count - 1), month, 98 - (7 * month));
                                            xlWorkSheetToUpload.Cells[num_row2, 99 - (7 * month)].Formula = this.rendervaluefrommonth((num_row2 + 1), (dt2.Rows.Count - 1), month, 99 - (7 * month));
                                            xlWorkSheetToUpload.Cells[num_row2, 100 - (7 * month)].Formula = this.rendervaluefrommonth((num_row2 + 1), (dt2.Rows.Count - 1), month, 100 - (7 * month));
                                            xlWorkSheetToUpload.Cells[num_row2, 101 - (7 * month)].Formula = this.rendervaluefrommonth((num_row2 + 1), (dt2.Rows.Count - 1), month, 101 - (7 * month));
                                            xlWorkSheetToUpload.Cells[num_row2, 102 - (7 * month)].Formula = this.rendervaluefrommonth((num_row2 + 1), (dt2.Rows.Count - 1), month, 102 - (7 * month));

                                            //for (int k = 0; k < dt.Rows.Count; k++)
                                            //{
                                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                                            xlWorkSheetToUpload.get_Range("A" + iRowCnt, "A" + iRowCnt).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                            xlWorkSheetToUpload.get_Range("C" + iRowCnt, "CQ" + iRowCnt).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                            //xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Value = "0";

                                            int k = 0;

                                                xlWorkSheetToUpload.Cells[iRowCnt, 1].value = dt.Rows[k]["PRS_NO"];
                                                xlWorkSheetToUpload.Cells[iRowCnt, 2].value = dt.Rows[k]["NAME"]; //ชื่อคน
                                                xlWorkSheetToUpload.Cells[iRowCnt, 3].value = dt.Rows[k]["WORKIN"].ToString().Substring(0, 5);
                                                xlWorkSheetToUpload.Cells[iRowCnt, 4].value = dt.Rows[k]["WORKOUT"].ToString().Substring(0, 5);
                                                xlWorkSheetToUpload.Cells[iRowCnt, 96 - (7 * month)].value = dt.Rows[k]["LEAVE"];
                                                xlWorkSheetToUpload.Cells[iRowCnt, 97 - (7 * month)].value = dt.Rows[k]["LATEMIN"];
                                                xlWorkSheetToUpload.Cells[iRowCnt, 98 - (7 * month)].value = dt.Rows[k]["LATENUM"];
                                                xlWorkSheetToUpload.Cells[iRowCnt, 99 - (7 * month)].value = dt.Rows[k]["SICK"];
                                                xlWorkSheetToUpload.Cells[iRowCnt, 100 - (7 * month)].value = dt.Rows[k]["BUSINESS"];
                                                xlWorkSheetToUpload.Cells[iRowCnt, 101 - (7 * month)].value = dt.Rows[k]["VACATION"];
                                                xlWorkSheetToUpload.Cells[iRowCnt, 102 - (7 * month)].value = dt.Rows[k]["OTHER"];

                                                xlWorkSheetToUpload.Cells[iRowCnt, 5].Formula = "=SUM(L" + iRowCnt + ",S" + iRowCnt + ",Z" + iRowCnt + ",AG" + iRowCnt + ",AN" + iRowCnt + ",AU" + iRowCnt + ",BB" + iRowCnt + ",BI" + iRowCnt + ",BP" + iRowCnt + ",BW" + iRowCnt + ",CD" + iRowCnt + ",CK" + iRowCnt + ")";
                                                xlWorkSheetToUpload.Cells[iRowCnt, 6].Formula = "=SUM(M" + iRowCnt + ",T" + iRowCnt + ",AA" + iRowCnt + ",AH" + iRowCnt + ",AO" + iRowCnt + ",AV" + iRowCnt + ",BC" + iRowCnt + ",BJ" + iRowCnt + ",BQ" + iRowCnt + ",BX" + iRowCnt + ",CE" + iRowCnt + ",CL" + iRowCnt + ")";
                                                xlWorkSheetToUpload.Cells[iRowCnt, 7].Formula = "=SUM(N" + iRowCnt + ",U" + iRowCnt + ",AB" + iRowCnt + ",AI" + iRowCnt + ",AP" + iRowCnt + ",AW" + iRowCnt + ",BD" + iRowCnt + ",BK" + iRowCnt + ",BR" + iRowCnt + ",BY" + iRowCnt + ",CF" + iRowCnt + ",CM" + iRowCnt + ")";
                                                xlWorkSheetToUpload.Cells[iRowCnt, 8].Formula = "=SUM(O" + iRowCnt + ",V" + iRowCnt + ",AC" + iRowCnt + ",AJ" + iRowCnt + ",AQ" + iRowCnt + ",AX" + iRowCnt + ",BE" + iRowCnt + ",BL" + iRowCnt + ",BS" + iRowCnt + ",BZ" + iRowCnt + ",CG" + iRowCnt + ",CN" + iRowCnt + ")";
                                                xlWorkSheetToUpload.Cells[iRowCnt, 9].Formula = "=SUM(P" + iRowCnt + ",W" + iRowCnt + ",AD" + iRowCnt + ",AK" + iRowCnt + ",AR" + iRowCnt + ",AY" + iRowCnt + ",BF" + iRowCnt + ",BM" + iRowCnt + ",BT" + iRowCnt + ",CA" + iRowCnt + ",CH" + iRowCnt + ",CO" + iRowCnt + ")";
                                                xlWorkSheetToUpload.Cells[iRowCnt, 10].Formula = "=SUM(Q" + iRowCnt + ",X" + iRowCnt + ",AE" + iRowCnt + ",AL" + iRowCnt + ",AS" + iRowCnt + ",AZ" + iRowCnt + ",BG" + iRowCnt + ",BN" + iRowCnt + ",BU" + iRowCnt + ",CB" + iRowCnt + ",CI" + iRowCnt + ",CP" + iRowCnt + ")";
                                                xlWorkSheetToUpload.Cells[iRowCnt, 11].Formula = "=SUM(R" + iRowCnt + ",Z" + iRowCnt + ",AF" + iRowCnt + ",AM" + iRowCnt + ",AT" + iRowCnt + ",BA" + iRowCnt + ",BH" + iRowCnt + ",BO" + iRowCnt + ",BV" + iRowCnt + ",CC" + iRowCnt + ",CJ" + iRowCnt + ",CQ" + iRowCnt + ")";

                                                //xlWorkSheetToUpload.get_Range(iRowCnt, 1).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                                //xlWorkSheetToUpload.get_Range(iRowCnt, 2).Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);


                                                //xlWorkSheetToUpload.get_Range("CK6", "CQ13").Font.Name = "Tahoma";
                                                //xlWorkSheetToUpload.get_Range("CK6", "CQ13").Font.Size = "15";

                                                //xlWorkSheetToUpload.get_range[iRowCnt, 96 - (7 * month)].Font.Size = 16;
                                                //xlWorkSheetToUpload.Cells[iRowCnt, 96 - (7 * month)].Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red);

                                                
                                            //}
                                        }
                                        //else
                                        //{
                                        //    xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
                                        //    xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                                        //    xlWorkSheetToUpload.get_Range("A" + iRowCnt, "A" + iRowCnt).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                        //    xlWorkSheetToUpload.get_Range("C" + iRowCnt, "CQ" + iRowCnt).HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                                        //    xlWorkSheetToUpload.get_Range("A" + iRowCnt, "CQ" + iRowCnt).Cells.Value = "0";
                                        //}
                                    }
                                    //iRowCnt = iRowCnt + 1;
                                }
                               }
                            iRowCnt = iRowCnt + 1;
                            //xlWorkSheetToUpload.Cells[(iRowCnt), 3].Formula = this.generateFormulaCounta("C", list);
                            //xlWorkSheetToUpload.Cells[(iRowCnt), 4].Formula = this.generateFormulaCounta("D", list);

                            if (row_group == 1)
                            {
                                xlWorkSheetToUpload.Rows[(row_month + 1) + ":" + (row_month + row_group)].Group();
                            }
                            else
                            {
                                xlWorkSheetToUpload.Rows[(row_month + 1) + ":" + (row_month + row_group)].Group();
                            }



                            for (int month=1;month <= 12; month++)
                            {
                                xlWorkSheetToUpload.Cells[num_row1, 96 - (7 * month)].Formula = this.generateFormulaMasterDept(96, month, list);
                                xlWorkSheetToUpload.Cells[num_row1, 97 - (7 * month)].Formula = this.generateFormulaMasterDept(97, month, list);
                                xlWorkSheetToUpload.Cells[num_row1, 98 - (7 * month)].Formula = this.generateFormulaMasterDept(98, month, list);
                                xlWorkSheetToUpload.Cells[num_row1, 99 - (7 * month)].Formula = this.generateFormulaMasterDept(99, month, list);
                                xlWorkSheetToUpload.Cells[num_row1, 100 - (7 * month)].Formula = this.generateFormulaMasterDept(100, month, list);
                                xlWorkSheetToUpload.Cells[num_row1, 101 - (7 * month)].Formula = this.generateFormulaMasterDept(101, month, list);
                                xlWorkSheetToUpload.Cells[num_row1, 102 - (7 * month)].Formula = this.generateFormulaMasterDept(102, month, list);

                            }
                        }

                        xlWorkSheetToUpload.Rows[(num_row1 + 1) + ":" + (num_row1 + rowHeadGroup)].Group();





                        for (int month = 1; month <= 12; month++)
                        {
                            xlWorkSheetToUpload.Cells[3, 96 - (7 * month)].Formula = this.generateFormulaMasterDept(96, month, listHead);
                            xlWorkSheetToUpload.Cells[3, 97 - (7 * month)].Formula = this.generateFormulaMasterDept(97, month, listHead);
                            xlWorkSheetToUpload.Cells[3, 98 - (7 * month)].Formula = this.generateFormulaMasterDept(98, month, listHead);
                            xlWorkSheetToUpload.Cells[3, 99 - (7 * month)].Formula = this.generateFormulaMasterDept(99, month, listHead);
                            xlWorkSheetToUpload.Cells[3, 100 - (7 * month)].Formula = this.generateFormulaMasterDept(100, month, listHead);
                            xlWorkSheetToUpload.Cells[3, 101 - (7 * month)].Formula = this.generateFormulaMasterDept(101, month, listHead);
                            xlWorkSheetToUpload.Cells[3, 102 - (7 * month)].Formula = this.generateFormulaMasterDept(102, month, listHead);

                        }
                    }

                    //.Cells[iRowCnt, num_row1].Cells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    


                    /*
                    xlWorkSheetToUpload.Cells[12, 1] = "DataCol 1";
                    xlWorkSheetToUpload.Cells[12, 2] = "DataCol 2";
                    xlWorkSheetToUpload.Cells[12, 3] = "DataCol 3";
                    xlWorkSheetToUpload.Cells[12, 4] = "DataCol 4";
                    xlWorkSheetToUpload.Cells[12, 5] = "DataCol 5";
                    
                    xlWorkSheetToUpload.Cells[12, 6] = "DataCol 6";
                    
                    xlWorkSheetToUpload.Cells[12, 7] = "DataCol 7";
                    
                    xlWorkSheetToUpload.Cells[12, 8] = "DataCol 8";
                    
                    xlWorkSheetToUpload.Cells[12, 9] = "DataCol 9"; */


                    /*******************template ****************/

                    //if (dt.Rows.Count > 0)
                    //{
                    //iRowCnt = 20;                      // ROW AT WHICH PRINT WILL START.

                    // SHOW THE HEADER BOLD AND SET FONT AND SIZE.
                    //xlWorkSheetToUpload.Cells[1, 1].value = "Employee Details";
                    //xlWorkSheetToUpload.Cells[1, 1].FONT.NAME = "Calibri";
                    //xlWorkSheetToUpload.Cells[1, 1].Font.Bold = true;
                    //xlWorkSheetToUpload.Cells[1, 1].Font.Size = 20;

                    // MERGE CELLS OF THE HEADER.
                    /* xlWorkSheetToUpload.Range["A1:E1"].MergeCells = true;

                    // SHOW COLUMNS ON THE TOP.
                    xlWorkSheetToUpload.Cells[iRowCnt - 1, 1].value = "NO";
                    xlWorkSheetToUpload.Cells[iRowCnt - 1, 2].value = "FIRSTNAME";
                    xlWorkSheetToUpload.Cells[iRowCnt - 1, 3].value = "LASTNAME";
                    xlWorkSheetToUpload.Cells[iRowCnt - 1, 4].value = "IDCARD";
                    xlWorkSheetToUpload.Cells[iRowCnt - 1, 5].value = "AGE";

                    // NOW WRITE DATA TO EACH CELL.
                    for (var i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        xlWorkSheetToUpload.Cells[iRowCnt, 1].value = i;
                        xlWorkSheetToUpload.Cells[iRowCnt, 2].value = dt.Rows[i]["PRS_NO"];
                        xlWorkSheetToUpload.Cells[iRowCnt, 3].value = dt.Rows[i]["NAME"];
                        xlWorkSheetToUpload.Cells[iRowCnt, 4].value = dt.Rows[i]["DEPT_CODE"];
                        xlWorkSheetToUpload.Cells[iRowCnt, 5].value = dt.Rows[i]["LEAVE"];
                        xlWorkSheetToUpload.Cells[iRowCnt, 6].value = dt.Rows[i]["SICK"];
                        iRowCnt = iRowCnt + 1;
                    } */


                    // FINALLY, FORMAT THE EXCEL SHEET USING EXCEL'S AUTOFORMAT FUNCTION.
                    //xlAppToUpload.ActiveCell.Worksheet.Cells[4, 1].AutoFormat(ExcelAutoFormat.xlRangeAutoFormatList3);




                    // SAVE THE FILE IN A FOLDER.
                    xlWorkSheetToUpload.SaveAs(path + "Monly_Test.xlsx");

                    // CLEAR.
                    xlAppToUpload.Workbooks.Close();
                    xlAppToUpload.Quit();
                    xlAppToUpload = null;
                    xlWorkSheetToUpload = null;

                    MessageBox.Show("OK", "Success",
                  MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "You got an Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sda.Dispose();
                    sda = null;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        public String generateFormulaMasterDept(int column, int month, ArrayList list)
        {
            String str = "=SUM(";
            for (var i = 0; i < list.Count; i++)
            {
                if (i == list.Count - 1)
                {
                    str += this.GetExcelColumnName(column - (7 * month)) + list[i].ToString();
                }
                else
                {
                    str += this.GetExcelColumnName(column - (7 * month)) + list[i].ToString() + ",";
                }
            }
            return str + ")";
        }

        private String generateFormulaCounta(int column, ArrayList list)
        {
            String str = "=COUNTA(";
            for(var i = 0; i< list.Count; i++)
            {
                if( i == list.Count - 1)
                {
                    str += column + list[i].ToString();
                }
                else
                {
                    str += column + list[i].ToString()+",";
                }
            }
            return str + ")";
        }


        private string GetExcelColumnName(int columnNumber)
        {
            string columnName = "";

            while (columnNumber > 0)
            {
                int modulo = (columnNumber - 1) % 26;
                columnName = Convert.ToChar('A' + modulo) + columnName;
                columnNumber = (columnNumber - modulo) / 26;
            }

            return columnName;
        }

        public string rendervaluefrommonth (int row , int column, int month,int columnNumber)
        {
            string formulamonth = "";
            /*if (month == 1)
            {
                formulamonth = "=SUM("+this.GetExcelColumnName(columnNumber) + row + ":"+ this.GetExcelColumnName(columnNumber) + (row + column) + ")";
            }
            else if (month == 2)
            {
                formulamonth = "=SUM(CD" + row + ":CD" + (row + column) + ")";
            }
            else if (month == 3)
            {
                formulamonth = "=SUM(BW" + row + ":BW" + (row + column) + ")";
            }
            else if (month == 4)
            {
                formulamonth = "=SUM(BP" + row + ":BP" + (row + column) + ")";
            }
            else if (month == 5)
            {
                formulamonth = "=SUM(BI" + row + ":BI" + (row + column) + ")";
            }
            else if (month == 6)
            {
                formulamonth = "=SUM(BB" + row + ":BB" + (row + column) + ")";
            }
            else if (month == 7)
            {
                formulamonth = "=SUM(AU" + row + ":AU" + (row + column) + ")";
            }
            else if (month == 8)
            {
                formulamonth = "=SUM(AN" + row + ":AN" + (row + column) + ")";
            }
            else if (month == 9)
            {
                formulamonth = "=SUM(AG" + row + ":AG" + (row + column) + ")";
            }
            else if (month == 10)
            {
                formulamonth = "=SUM(Z" + row + ":Z" + (row + column) + ")";
            }
            else if (month == 11)
            {
                formulamonth = "=SUM(S" + row + ":S" + (row + column) + ")";
            }
            else if (month == 12)
            {
                formulamonth = "=SUM(L" + row + ":L" + (row + column) + ")";
            }*/

            return "=SUM(" + this.GetExcelColumnName(columnNumber) + row + ":" + this.GetExcelColumnName(columnNumber) + (row + column) + ")";
        }
        
        public string sqlquery(string dept_code, int month, int year, string prs_no)
        {
            string sql = " SELECT TEMP.PRS_NO, NAME, WORKIN, WORKOUT,DEPT_CODE, SUM(LEAVE) AS LEAVE, CAST(CAST((SUM(LATEMIN/60)) AS int)AS varchar)+':' + CAST(CAST((SUM(LATEMIN)%60)AS INT)AS varchar)  AS LATEMIN , ";
            sql += " SUM(LATENUM)  AS LATENUM, SUM(SICK)  AS SICK, SUM(BUSINESS) AS BUSINESS, SUM(VACATION)  AS VACATION, SUM(OTHER)  AS OTHER  ";
            sql += " FROM ";
            sql += " (SELECT ";
            sql += " PRS_NO, EMP_INTL + ' ' + EMP_NAME + ' ' + EMP_SURNME AS NAME, ";
            sql += " tmshift.SF_IN_TIME AS WORKIN, ";
            sql += " tmshift.SF_OUT_TIME AS WORKOUT, ";
            sql += " DEPT_CODE, ";
            sql += " 1 AS LEAVE, ";
            sql += " 0 AS LATENUM, ";
            sql += " 0 AS LATEMIN, ";
            sql += " 0 AS VACATION, ";
            sql += " 0 AS SICK, ";
            sql += " 0 AS BUSINESS, ";
            sql += " 0 AS OTHER ";
            sql += " FROM ";
            sql += " TMRESULT, EMPFILE, PERSONALINFO, PAYROLLINFO, DEPTTAB, TMTRAN, TMSHIFT, PRDEFTAB, TMSTAMPTYPE, BRANCH ";
            sql += " WHERE ";
            sql += " (TMRESULT.TMR_EMP = PERSONALINFO.PRS_EMP) AND ";
            sql += " (TMRESULT.TMR_EMP = PAYROLLINFO.PRI_EMP) AND ";
            sql += " (TMRESULT.TMR_EMP = EMPFILE.EMP_KEY) AND ";
            sql += " (TMRESULT.TMR_EMP = TMTRAN.TMT_EMP) AND ";
            sql += " (PERSONALINFO.PRS_DEPT = DEPTTAB.DEPT_KEY) AND ";
            sql += " (TMRESULT.TMR_SF = TMSHIFT.SF_KEY) AND ";
            sql += " (TMRESULT.TMR_SF = TMTRAN.TMT_SF) AND ";
            sql += " (TMRESULT.TMR_DATE = TMTRAN.TMT_DATE) AND ";
            sql += " (TMRESULT.TMR_DF_T = PRDEFTAB.DF_KEY) AND ";
            sql += " (TMRESULT.TMR_STT = TMSTAMPTYPE.STT_KEY) ";
            sql += " AND(PERSONALINFO.PRS_BR = BRANCH.BR_KEY) ";
            sql += " AND SF_CODE = 'SD1' ";
            sql += " AND TMT_WORK_HOUR = 0.00 ";
            sql += " AND month(TMR_DATE) = " + month + " ";
            sql += " AND year(TMR_DATE) = " + year + " ";
            sql += " AND TMR_DF_T = 211 ";
            sql += " AND DEPT_CODE <> 3000 AND(DEPT_CODE <> '1403' OR PRS_NO = 'SV0004') ";
            sql += " AND(DEPT_CODE <> '1800' OR PRS_NO <> 'PAT20') ";
            sql += " UNION ALL  ";
            sql += " SELECT DISTINCT  ";
            sql += " PRS_NO, EMP_INTL + ' ' + EMP_NAME + ' ' + EMP_SURNME AS NAME, ";
            sql += " tmshift.SF_IN_TIME AS WORKIN, ";
            sql += " tmshift.SF_OUT_TIME AS WORKOUT, ";
            sql += " DEPTTAB.DEPT_CODE, ";
            sql += " 0 AS LEAVE, ";
            sql += " COUNT(TMR_QTY_T)AS LATENUM, ";
            sql += " ROUND(SUM(TMR_QTY_T * 60), 0) AS LATEMIN, ";
            sql += " 0 AS VACATION, ";
            sql += " 0 AS SICK, ";
            sql += " 0 AS BUSINESS, ";
            sql += " 0 AS OTHER ";
            sql += " FROM ";
            sql += " TMRESULT, EMPFILE, PERSONALINFO, PAYROLLINFO, DEPTTAB, TMTRAN, TMSHIFT, PRDEFTAB, TMSTAMPTYPE, BRANCH ";
            sql += " WHERE ";
            sql += " (TMRESULT.TMR_EMP = PERSONALINFO.PRS_EMP) AND ";
            sql += " (TMRESULT.TMR_EMP = PAYROLLINFO.PRI_EMP) AND ";
            sql += " (TMRESULT.TMR_EMP = EMPFILE.EMP_KEY) AND ";
            sql += " (TMRESULT.TMR_EMP = TMTRAN.TMT_EMP) AND ";
            sql += " (PERSONALINFO.PRS_DEPT = DEPTTAB.DEPT_KEY) AND ";
            sql += " (TMRESULT.TMR_SF = TMSHIFT.SF_KEY) AND ";
            sql += " (TMRESULT.TMR_SF = TMTRAN.TMT_SF) AND ";
            sql += " (TMRESULT.TMR_DATE = TMTRAN.TMT_DATE) AND ";
            sql += " (TMRESULT.TMR_DF_T = PRDEFTAB.DF_KEY) AND ";
            sql += " (TMRESULT.TMR_STT = TMSTAMPTYPE.STT_KEY) ";
            sql += " AND(PERSONALINFO.PRS_BR = BRANCH.BR_KEY) ";
            sql += " AND SF_CODE = 'SD1' ";
            sql += " AND month(TMR_DATE) = " + month + " ";
            sql += " AND year(TMR_DATE) = " + year + " ";
            sql += " AND TMR_DF_T = 264 ";
            sql += " AND DEPT_CODE <> 3000 AND(DEPT_CODE <> '1403' OR PRS_NO = 'SV0004') ";
            sql += " AND(DEPT_CODE <> '1800' OR PRS_NO <> 'PAT20') ";
            sql += " GROUP BY PRS_NO, EMP_INTL + ' ' + EMP_NAME + ' ' + EMP_SURNME, tmshift.SF_IN_TIME, tmshift.SF_OUT_TIME, DEPTTAB.DEPT_CODE ";
            sql += " UNION ALL ";
            sql += " SELECT ";

            sql += " a.PRS_NO AS CODE, ";
            sql += " b.EMP_INTL + ' ' + b.EMP_NAME + ' ' + b.EMP_SURNME AS NAME, ";
            sql += " tmshift.SF_IN_TIME AS WORKIN, ";
            sql += " tmshift.SF_OUT_TIME AS WORKOUT, ";
            sql += " DEPT_CODE, ";
            sql += " 0 AS LEAVE, ";
            sql += " 0 AS LATENUM, ";
            sql += " 0 AS LATEMIN, ";
            sql += " (SELECT COUNT(*) FROM dbo.TMTRAP t1 INNER JOIN dbo.TMSTAMPTYPE t2 ON t1.TMP_STT = t2.STT_KEY  WHERE a.PRS_EMP = t1.TMP_EMP AND t2.STT_CODE = '020010(v5.0)'  AND MONTH(t1.TMP_DATE) = " + month + " AND YEAR(t1.TMP_DATE) = " + year + ") AS VACATION, ";
            sql += " (SELECT COUNT(*) FROM dbo.TMTRAP t1 INNER JOIN dbo.TMSTAMPTYPE t2 ON t1.TMP_STT = t2.STT_KEY  WHERE a.PRS_EMP = t1.TMP_EMP AND(t2.STT_CODE = '020007(v5.0)' OR t2.STT_CODE = '020008(v5.0)') AND MONTH(t1.TMP_DATE) = " + month + " AND YEAR(t1.TMP_DATE) = " + year + " ) AS SICK, ";
            sql += " (SELECT COUNT(*) FROM dbo.TMTRAP t1 INNER JOIN dbo.TMSTAMPTYPE t2 ON t1.TMP_STT = t2.STT_KEY  WHERE a.PRS_EMP = t1.TMP_EMP AND t2.STT_CODE = '020009(v5.0)' AND MONTH(t1.TMP_DATE) = " + month + " AND YEAR(t1.TMP_DATE) = " + year + " ) AS BUSINESS, ";
            sql += " (SELECT COUNT(*) FROM dbo.TMTRAP t1 INNER JOIN dbo.TMSTAMPTYPE t2 ON t1.TMP_STT = t2.STT_KEY  WHERE a.PRS_EMP = t1.TMP_EMP AND NOT(t2.STT_CODE = '020009(v5.0)' OR  t2.STT_CODE = '020008(v5.0)' OR  t2.STT_CODE = '020007(v5.0)' OR  t2.STT_CODE = '020010(v5.0)' OR t2.STT_CODE = '000002') AND MONTH(t1.TMP_DATE) = " + month + " AND YEAR(t1.TMP_DATE) = " + year + " ) AS OTHER ";

            sql += " FROM ";

            sql += " dbo.PERSONALINFO AS a ";
            sql += " LEFT OUTER JOIN dbo.EMPFILE AS b ON a.PRS_EMP = b.EMP_KEY ";

            sql += " LEFT OUTER JOIN dbo.TMWORKTAB AS tmwktab ON a.PRS_WT = tmwktab.WT_KEY ";

            sql += " LEFT OUTER JOIN dbo.TMSHIFT AS tmshift ON tmwktab.WT_MON_SF = tmshift.SF_KEY ";

            sql += " LEFT OUTER JOIN dbo.TMTRAP AS ap ON a.PRS_EMP = ap.TMP_EMP ";

            sql += " LEFT OUTER JOIN dbo.TMSTAMPTYPE AS aptype ON ap.TMP_STT = aptype.STT_KEY ";

            sql += " LEFT OUTER JOIN dbo.DEPTTAB AS DEPT ON a.PRS_DEPT = DEPT.DEPT_KEY ";


            sql += " WHERE ";
            sql += " (ap.TMP_DATE IS NOT NULL) ";

            sql += " AND(aptype.STT_DESC <> '') ";

            sql += " AND MONTH(TMP_DATE) = " + month + " ";

            sql += " AND YEAR(TMP_DATE) = " + year + " ";
            sql += " AND DEPT_CODE<> 3000 AND(DEPT_CODE <> '1403' OR PRS_NO = 'SV0004') ";

            sql += " AND(DEPT_CODE <> '1800' OR PRS_NO <> 'PAT20') ";

            sql += " GROUP BY PRS_EMP, a.PRS_NO,(b.EMP_INTL + ' ' + b.EMP_NAME + ' ' + b.EMP_SURNME), tmshift.SF_IN_TIME, tmshift.SF_OUT_TIME,DEPT_CODE ";
            sql += " UNION ALL SELECT DISTINCT CODE, NAME, WORKIN, WORKOUT, DEPT_CODE, ";
            sql += " 0 AS LEAVE, ";
            sql += " 0 AS LATENUM, ";
            sql += " 0 AS LATEMIN, ";
            sql += " 0 AS VACATION, ";
            sql += " 0 AS SICK, ";
            sql += " 0 AS BUSINESS, ";
            sql += " 0 AS OTHER ";
            sql += " FROM VW_HR_REPORT ";
            sql += " WHERE month(WORKDATE) = " + month + " ";
            sql += " AND year(WORKDATE) = " + year + " ";
            sql += " AND LEFT(CODE,2) <> 'PC' ";
            sql += " AND DEPT_CODE<> 3000 AND(DEPT_CODE <> '1403' OR CODE = 'SV0004') ";
            sql += " AND(DEPT_CODE <> '1800' OR CODE <> 'PAT20') ";
            sql += " ) AS TEMP ";
            sql += " WHERE PRS_NO = '" + prs_no + "' ";
            sql += " GROUP BY  PRS_NO, NAME, WORKIN, WORKOUT,DEPT_CODE ";
            return sql;


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
